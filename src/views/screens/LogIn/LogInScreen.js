import React from "react";
import {
    View, Text, Image, Button, TouchableOpacity, TextInput,
    KeyboardAvoidingView, Platform, Dimensions, Modal, ScrollView
} from "react-native";
import LogIn_Header from "../../../assets/images/Login_Header.png"
import Logo from "../../../assets/images/Logo.png"
import TxtInputComponent from "../../../components/TxtInputComponent";
import BtnComponent from "../../../components/BtnComponent";
import { useRef, useState } from "react";
import RBSheet from "react-native-raw-bottom-sheet";
import OtpInputs from "react-native-otp-inputs";
import styles from "./styles";
import { LogBox } from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";
import { useHeaderHeight } from '@react-navigation/elements'
LogBox.ignoreLogs(['new NativeEventEmitter']);
export default function LogIn(props) {
    const [phone, setPhone] = useState()
    const refRBSheet = useRef();
    const { width, height } = Dimensions.get('window');
    return (
            <View style={styles.container}>
                <Image style={{height:height*.8, width:width}} source={LogIn_Header} />
                <Modal transparent={true}>
                    <View style={styles.logIn}>
                        <Text style={styles.welcomeTxt}>Chào mừng bạn đến với </Text>
                        <Image style={{marginTop: 16}}
                            source={Logo} />
                        <KeyboardAvoidingView
                            style={{ flex: 1, justifyContent:"flex-end", marginBottom:height*0.05}}>
                            <TxtInputComponent label="phoneLI" startIcon={'telephone'}
                                value={phone} onChangeText={setPhone}
                                keyboardType={Platform.OS === 'android' ? "numeric" : "number-pad"} />
                            <BtnComponent size="btnLogIn"
                                onPress={() => refRBSheet.current.open()} />
                        </KeyboardAvoidingView>
                    </View>
                </Modal>
                <RBSheet
                    ref={refRBSheet}
                    customStyles={{
                        container:{ height:height * 0.8, alignItems: "center", backgroundColor: "#114459F2" }}}>
                    {/* <YourOwnComponent /> */}
                    <BtnComponent size="btnX" />
                    <Text style={{fontSize: 20, color: '#BF946A', fontWeight: "700", marginTop: 85}}>
                        Xác nhận mã OTP</Text>
                    <Text style={styles.notiTxt}>Một mã xác thực
                        gồm 4 số đã được
                        gửi đến số điện thoại 0909909099</Text>
                    <OtpInputs style={styles.otpInput}
                        numberOfInputs={4}
                        inputStyles={styles.underlineStyleBase}
                        handleChange={(otpCode) => {
                            if (otpCode.length == 4)
                                props.navigation.navigate('SignUp')
                        }}/>
                    <Text style={styles.notiTxt}>Nhận mã để tiếp tục </Text>
                    <View style={{ flexDirection: "row", alignItems: "center",  marginTop: 32 }}>
                        <Text style={styles.notOtpTxt}>Bạn không nhận được mã?</Text>
                        <BtnComponent size="btnResend" />
                        <Text style={styles.secTxt}>(Giây)</Text>
                    </View>
                </RBSheet>
            </View>
    )
}