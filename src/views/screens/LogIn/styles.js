import { StyleSheet, Dimensions } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
// screen sizing
const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const numColumns = 3;
// item size
const RECIPE_ITEM_HEIGHT = 100;
const RECIPE_ITEM_OFFSET = 10;
const RECIPE_ITEM_MARGIN = RECIPE_ITEM_OFFSET * 2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 600,
    borderRadius: 15,
  },
  logIn: {
    backgroundColor: '#114459F2', flex: 1,
    alignItems: "center", marginTop: height * .45
  },
  welcomeTxt: {
    width: 136, height: 20,
    fontSize: 16, fontWeight: "500",
    color: 'white', marginTop: 30
  },
  underlineStyleBase: {
    textAlign: 'center',
    marginLeft: 26,
    minWidth: 30,
    minHeight: 30,
    borderWidth: 3,
    borderColor: 'white',
    borderRadius: 5,
    color: 'white',
  },
  notiTxt: {
    fontSize: 16, color: 'white', fontWeight: "400", marginTop: 24,
    width: 329, textAlign: "center"
  },
  otpInput: {
    flexDirection: "row", marginRight: 26,
    width: 55, height: 40, justifyContent: "center",
    borderColor: 'cyan',
    marginTop: 61, backgroundColor: "#114459F2"
  },
  notOtpTxt: {
    fontSize: 16, color: 'white',
    fontWeight: "400",
    width: 153
  },
  secTxt: {
    fontSize: 16, color: 'white',
    fontWeight: "400",
    width: 50,
  }


});

export default styles;
