import React, { useState } from "react";
import { View, Text, Image, ScrollView, Dimensions, TouchableOpacity, TextInputComponent } from "react-native";
import styles from "./styles";
import BtnComponent from "../../../components/BtnComponent";
import QRCode from "../../../assets/images/QR.png"
import { SliderBox } from "react-native-image-slider-box";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import Ionicons from "react-native-vector-icons/Ionicons"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import Home_Icon from "../../../assets/icons/home.png"
import Order_Icon from "../../../assets/icons/order.png"
import Store_Icon from "../../../assets/icons/store.png"
import Promos_Icon from "../../../assets/icons/promos.png"
import Account_Icon from "../../../assets/icons/account.png"
import Latte from "../../../assets/images/Latte.png"
import SearchBar from "../../../components/Search/SearchBar";
import TxtInputComponent from "../../../components/TxtInputComponent";
import map from '../../../assets/icons/map.png'
import Store from '../../../assets/images/Store.png'
import deli from '../../../assets/icons/deli.png'
// import AccumulatePtsLogo from "assets/images/AccumulatePtsLogo.png"
import AccumulatePtsLogo from "../../../assets/images/AccumulatePtsLogo.png"

// import { TabView, SceneMap } from "react-native-tab-view";
import { TabView, SceneMap } from "react-native-tab-view";
import Promotions from "../../../components/TabView/Promotions";
const state = {
  images: [

    "https://images.foody.vn/res/g114/1135437/prof/s640x400/foody-upload-api-foody-mobile-oo-d053e55f-221129102024.jpeg",
    "https://images.foody.vn/res/g114/1135437/prof/s1242x600/image-4974350c-220930131908.jpg",
    require('../../../assets/images/Banner.png'),          // Local image
  ],
  promosImgs: [
    "https://promotion.zalopay.vn/static/promotion/1x_Banner_Katinat_Old.jpg",
    require('../../../assets/images/Momo_Promos.png'),          // Local image
  ]
};
const Tab = createBottomTabNavigator();
function HomeScreen() {
  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.greeting}>Chào Su, coffee thôi nàoo!</Text>
        <BtnComponent size="btnNoti" />
        <BtnComponent size="btnPromos" />
        <View style={styles.header}>
          <Image source={QRCode} />
        </View>
        <Text style={styles.promos}>Ưu đãi đặc biệt hôm nay</Text>
        <BtnComponent size="btnAll" />
        <View style={{ marginTop: 64, width: 320 }}>
          <SliderBox
            //ImageComponent={FastImage}
            images={state.images}
            sliderBoxHeight={180}
            onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
            dotColor="white"
            inactiveDotColor="#90A4AE"
            paginationBoxVerticalPadding={20}
            // autoplay
            circleLoop
            resizeMethod={'resize'}
            resizeMode={'cover'}
            paginationBoxStyle={styles.paginationBox}
            dotStyle={styles.dot}
            ImageComponentStyle={{ borderRadius: 15, width: '95%' }}
            imageLoadingColor="#2196F3"
          />
        </View>
        <Text style={styles.FeaturedProducts}>Sản phẩm nổi bật</Text>
        <ScrollView style={{ marginTop: 50, }}
          horizontal
          disableIntervalMomentum={true}
          showsHorizontalScrollIndicator={false}
        >
          <BtnComponent size="btnCoffee" />
          <BtnComponent size="btnCoffee" />
          <BtnComponent size="btnCoffee" />
          <BtnComponent size="btnCoffee" />

        </ScrollView>
        <Text style={styles.SuggestedProducts}>Gợi ý dành riêng cho Su</Text>
        <ScrollView style={{ marginTop: 50, }}
          disableIntervalMomentum={true}
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          <View>
            <BtnComponent size="btnCoffee" />
            <BtnComponent size="btnPick" />
          </View>
          <View >
            <BtnComponent size="btnCoffee" />
            <BtnComponent size="btnPick" />
          </View>
          <View >
            <BtnComponent size="btnCoffee" />
            <BtnComponent size="btnPick" />
          </View>
          <View >
            <BtnComponent size="btnCoffee" />
            <BtnComponent size="btnPick" />
          </View>

        </ScrollView>
        <Text style={styles.news}>Tin tức</Text>
        <View style={{
          marginTop: 64, width: 320,
          marginBottom: 40
        }}>
          <SliderBox
            //ImageComponent={FastImage}
            images={state.promosImgs}
            sliderBoxHeight={180}
            onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
            dotColor="white"
            inactiveDotColor="#90A4AE"
            paginationBoxVerticalPadding={20}
            circleLoop
            resizeMethod={'resize'}
            resizeMode={'cover'}
            paginationBoxStyle={styles.paginationBox}
            dotStyle={styles.dot}
            ImageComponentStyle={{ borderRadius: 15, width: '95%' }}
            imageLoadingColor="#2196F3"
          />
        </View>
      </View>
    </ScrollView>


  );
}
function OrderScreen() {
  return (
    <View>
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.menu}>Danh mục</Text>
          <ScrollView style={{ marginTop: 12, }}
            horizontal
            disableIntervalMomentum={true}
            showsHorizontalScrollIndicator={false}
          >
            <BtnComponent size="btnCircleCoffee" />
            <BtnComponent size="btnCircleIceBlended" />
            <BtnComponent size="btnCircleTea" />
            <BtnComponent size="btnCircleMilkTea" />
            <BtnComponent size="btnCircleCake" />
          </ScrollView>
          <Text style={styles.promoProduct}>Ưu đãi các sản phẩm</Text>
          <View style={{ marginTop: 8, width: '90%' }}>
            <SliderBox
              //ImageComponent={FastImage}
              images={state.promosImgs}
              sliderBoxHeight={180}
              onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
              dotColor="white"
              inactiveDotColor="#90A4AE"
              paginationBoxVerticalPadding={20}
              circleLoop
              resizeMethod={'resize'}
              resizeMode={'cover'}
              paginationBoxStyle={styles.paginationBox}
              dotStyle={styles.dot}
              ImageComponentStyle={{ borderRadius: 15, width: '90%' }}
              imageLoadingColor="#2196F3"
            />
          </View>
          <Text style={styles.coffee} >Cà phê</Text>
          <View style={styles.productItem}>
            <Image source={Latte} />
            <View style={styles.txtEle}>
              <Text style={styles.titleEle}>Cà phê sữa</Text>
              <Text style={styles.priceEle}>35.000đ</Text>
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <BtnComponent size="btnAdd" />
            </View>
          </View>
          <View style={styles.productItem}>
            <Image source={Latte} />
            <View style={styles.txtEle}>
              <Text style={styles.titleEle}>Cà phê sữa</Text>
              <Text style={styles.priceEle}>35.000đ</Text>
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <BtnComponent size="btnAdd" />
            </View>
          </View>
          <View style={styles.productItem}>
            <Image source={Latte} />
            <View style={styles.txtEle}>
              <Text style={styles.titleEle}>Cà phê sữa</Text>
              <Text style={styles.priceEle}>35.000đ</Text>
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <BtnComponent size="btnAdd" />
            </View>
          </View>
          <Text style={styles.coffee} >Đá xay</Text>
          <View style={styles.productItem}>

            <Image source={Latte} />
            <View style={styles.txtMatcha}>
              <Text style={styles.titleEle}>Matcha đá xay</Text>
              <Text style={styles.priceEle}>35.000đ</Text>
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <BtnComponent size="btnAdd" />
            </View>
          </View>
          <View style={styles.productItem}>
            <Image source={Latte} />
            <View style={styles.txtMatcha}>
              <Text style={styles.titleEle}>Matcha đá xay</Text>
              <Text style={styles.priceEle}>35.000đ</Text>
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <BtnComponent size="btnAdd" />
            </View>
          </View>
          <View style={styles.productItem}>
            <Image source={Latte} />
            <View style={styles.txtMatcha}>
              <Text style={styles.titleEle}>Matcha đá xay</Text>
              <Text style={styles.priceEle}>35.000đ</Text>
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <BtnComponent size="btnAdd" />
            </View>
          </View>
        </View>

      </ScrollView>
      {/* Deli */}
      <TouchableOpacity style={{
        width: 420, height: 70,
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'white',
        flex: 1,
        justifyContent: "center",
        padding: 10,
        borderTopWidth: 0.2
      }} >
        <View style={{ flexDirection: "row" }}>
          <Image style={{ width: 20, height: 20 }} source={deli} />
          <Text style={{
            paddingLeft: 5, fontWeight: 'bold',
            width: 52, textAlignVertical: "center"
          }}>
            Giao đến
          </Text>
        </View>

        <Text style={{ color: 'black', fontWeight: 'bold', paddingTop: 5 }}>
          3 Sông Thao, Phường 2, Tân Bình, Thành Phố Hồ Ch...
        </Text>
      </TouchableOpacity>
    </View>

  );
}

function StoreScreen() {
  const [search, setSearch] = useState()
  return (
    <ScrollView>
      <Text style={styles.greeting}>Cửa hàng</Text>
      <BtnComponent size="btnNoti" />
      <BtnComponent size="btnPromos" />
      {/* Search Bar                   */}
      <View style={styles.search}>
        <TxtInputComponent label='searchBar' value={search} onChangeText={setSearch}/>
        <Image style={{ marginLeft: 18 }} source={map} />
        <Text style={styles.map}>Bản đồ</Text>
      </View>
      <View style={styles.store}>
        <Text style={styles.nearestStore}>Cửa hàng gần bạn</Text>
        <View style={styles.storeList}>
          <Image style={{ borderRadius: 15, marginLeft: 17 }} source={Store} />
          <View style={{ marginLeft: 24 }}>
            <Text style={styles.katinat}>Katinat Sai Gon Cafe</Text>
            <Text style={styles.address}>91 Đồng Khởi</Text>
            <Text style={styles.distance}>Cách đây 0.2km</Text>
          </View>
        </View>
      </View>
      <View style={styles.othStore}>
        <Text style={styles.otherstStore}>Các cửa hàng khác</Text>
        <View style={styles.othersStoreList}>
          <Image style={{ borderRadius: 15, marginLeft: 17 }} source={Store} />
          <View style={{ marginLeft: 24 }}>
            <Text style={styles.katinat}>Katinat Sai Gon Cafe</Text>
            <Text style={styles.address}>91 Đồng Khởi</Text>
            <Text style={styles.distance}>Cách đây 0.2km</Text>
          </View>
        </View>
        <View style={styles.othersStoreList}>
          <Image style={{ borderRadius: 15, marginLeft: 17 }} source={Store} />
          <View style={{ marginLeft: 24 }}>
            <Text style={styles.katinat}>Katinat Sai Gon Cafe</Text>
            <Text style={styles.address}>91 Đồng Khởi</Text>
            <Text style={styles.distance}>Cách đây 0.2km</Text>
          </View>
        </View>
        <View style={styles.othersStoreList}>
          <Image style={{ borderRadius: 15, marginLeft: 17 }} source={Store} />
          <View style={{ marginLeft: 24 }}>
            <Text style={styles.katinat}>Katinat Sai Gon Cafe</Text>
            <Text style={styles.address}>91 Đồng Khởi</Text>
            <Text style={styles.distance}>Cách đây 0.2km</Text>
          </View>
        </View>
        <View style={styles.othersStoreList}>
          <Image style={{ borderRadius: 15, marginLeft: 17 }} source={Store} />
          <View style={{ marginLeft: 24 }}>
            <Text style={styles.katinat}>Katinat Sai Gon Cafe</Text>
            <Text style={styles.address}>91 Đồng Khởi</Text>
            <Text style={styles.distance}>Cách đây 0.2km</Text>
          </View>
        </View>
      </View>

    </ScrollView>
  );
}
function PromosScreen() {
  const listTab = [
    {
      id: 0,
      status: 'Tích điểm'
    },
    {
      id: 1,
      status: 'Đổi ưu đãi'
    },
  ]
  const [status, setStatus] = useState('Tích điểm')
  const [tabContent, setTab] = useState(<Promotions/>)
  const setStatusFilter = id => {
    if (id === 1) {
      setStatus('Đổi ưu đãi')
      setTab(<View >
        <Text>abc</Text>
      </View>)
    }
    else {
      setStatus('Tích điểm')
      setTab(<Promotions />)
    }


  }
  return (
    <ScrollView>
      <View style={styles.bannerPromosScreen}>
        <View style={{
          flexDirection: "row", marginTop: 10, alignItems: "center",
        }}>
          <Text style={styles.txtPromos}>Ưu đãi</Text>
          <BtnComponent size="Promotions" />
          <BtnComponent size="Notifications" />
        </View>
        <View style={styles.listTab}>
          {
            listTab.map(e => (
              <TouchableOpacity style={[styles.btnTab,
              status === e.status &&
              styles.btnTabActive]}
                onPress={() => setStatusFilter(e.id)}>
                <Text style={[styles.txtTab,
                status === e.status &&
                styles.txtTabActive]}> {e.status} </Text>
              </TouchableOpacity>
            ))
          }
        </View>
      
      </View>
  

      {tabContent}
    </ScrollView>
  )
}

function AccountScreen() {
  return (
    <View>
      <Text>abc</Text>
    </View>
  )
}
export default function App() {

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Order') {
            iconName = 'food';
          } else if (route.name === 'Store') {
            iconName = 'store';
          } else if (route.name === 'Promos') {
            iconName = 'book';
          } else if (route.name === 'Account') {
            iconName = 'account';
          }
          // You can return any component that you like here!
          return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: '#BF946A',
        tabBarInactiveTintColor: 'gray',
      })}
    >
      <Tab.Screen

        name="Home" component={HomeScreen} />
      <Tab.Screen

        name="Order" component={OrderScreen} />
      <Tab.Screen
        name="Store" component={StoreScreen} />
      <Tab.Screen

        name="Promos" component={PromosScreen} />
      <Tab.Screen
        name="Account" component={AccountScreen} />
    </Tab.Navigator>
  );
}

