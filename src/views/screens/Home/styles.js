import { Dimensions, StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  container: {
    // alignItems: 'center',
    // borderRadius: 15,
    // shadowColor: 'grey',
    
  },
  greeting: {
    position: 'absolute',
    top: 19,
    left: 20,
    fontSize: 17, color: '#114459',
    fontWeight: "700",
    width:164
  },
  header: {
    width:Dimensions.get('window').width*0.95, height: 184,
    marginTop: 60,
    backgroundColor: 'rgba(17, 68, 89, 0.83)',
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 28,
    marginHorizontal:7,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,

    elevation: 18,
  },
  FeaturedProducts: {
    position: 'absolute',
    top: 505,
    left: 20,
    fontSize: 17, color: '#114459',
    fontWeight: "700"
  },
  SuggestedProducts: {
    position: 'absolute',
    top: 725,
    left: 20,
    fontSize: 17, color: '#114459',
    fontWeight: "700"
  },
  news: {
    position: 'absolute',
    top: 1000,
    left: 20,
    fontSize: 17, color: '#114459',
    fontWeight: "700"
  },
  promos: {
    position: 'absolute',
    top: 270,
    left: 20,
    fontSize: 17, color: '#114459',
    fontWeight: "700",
    width:155
  },
  all: {
    position: 'absolute',
    top: 270,
    right: 20,
    fontSize: 17, color: '#BF946A',
    fontWeight: "700",
  },
  menu: {
   paddingLeft:20,
   paddingTop:20,
    fontSize: 17, color: '#114459',
    fontWeight: "700"
  },
  promoProduct: {
    paddingLeft:20,
    paddingTop:30,
    fontSize: 17, color: '#114459',
    fontWeight: "700"
  },
  paginationBox: {
    bottom: 0,
    padding: 0,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    paddingVertical: 0,
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    padding: 0,
    margin: 5,
    backgroundColor: "rgba(128, 128, 128, 0.92)"
  },
  coffee: {
    paddingLeft:20,
    paddingTop: 27,
    marginBottom: 13,
    fontSize: 17,
    color: '#114459',
    fontWeight: "700",
  },
  productItem: {
    flex:1,
    flexDirection: "row",
    marginTop: 12,
    marginBottom: 12,
     paddingHorizontal:20
  },
  txtEle: {
    flexDirection: "column",
    marginRight: 20,
    marginLeft: 16,
  },
  txtMatcha: {
    flexDirection: "column",
    marginLeft: 16
  },
  titleEle: {
    fontSize: 17,
    color: 'black',
    fontWeight: "700",
    width:92
  },
  priceEle: {
    fontSize: 12,
    color: 'black',
    fontWeight: "400",
  },
  search: {
    marginLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    marginTop: 65,

  },
  map: {
    width: 56,
    height: 25,
    textAlign: "center",
    textAlignVertical: "center",
    fontWeight: '400',
    color: 'black'
  },
  store: {

    marginLeft: 20,
    marginTop: 20,
  },
  othStore: {

    marginLeft: 20,
    marginTop: 16,
  },
  nearestStore: {
    fontSize: 17,
    color: '#114459',
    fontWeight: "700",
  },
  otherstStore: {
    fontSize: 17,
    color: '#114459',
    fontWeight: "700",
    marginBottom: 16
  },
  storeList: {
    alignItems: 'center',
    backgroundColor: "#ffffff",
    maxWidth: 387,
    minHeight: 108,
    flexDirection: 'row',
    marginTop: 16,
    borderRadius: 20,
    marginRight: 21
  },
  othersStoreList: {
    alignItems: 'center',
    backgroundColor: "#ffffff",
    maxWidth: 387,
    minHeight: 108,
    flexDirection: 'row',
    marginBottom: 9,
    borderRadius: 20,
    marginRight: 21
  },
  katinat: {
    fontSize: 16,
    color: '#5A5D5F',
    fontWeight: "400",
  },
  address: {
    fontSize: 18,
    color: '#000000',
    fontWeight: "500",
  },
  distance: {
    fontSize: 16,
    color: '#5A5D5F',
    fontWeight: "400",
    marginTop: 20
  },
  txtPromos: {
    marginLeft: 20,
    fontSize: 17,
    color: '#114459',
    fontWeight: "700",
    width:41

  },
  listTab: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 10
  },
  btnTab: {
    width: Dimensions.get('window').width / 3.5,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: '#EBEBEB',
    padding: 10,
    justifyContent: 'center'
  },
  txtTab: {
    fontSize: 16,
    color: 'black'
  },
  btnTabActive: {
    backgroundColor: '#78B1C9',
    borderRadius: 18
  },
  txtTabActive: {
    color: 'white'
  },
  bannerPromosScreen: {
    backgroundColor: 'white',
    width: Dimensions.get('window').width,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 7
  },
  banner: {
    width: Dimensions.get('window').width*0.9, height: 200,
    backgroundColor: 'white',
    borderRadius: 18,
    marginTop: 142,
    marginHorizontal:15,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 0,
    
},
rank: {
    flexDirection: "row",
    marginTop: 70,
    paddingHorizontal: 10,
    elevation:13
},
txtRank: {
    color: 'black',
    fontSize: 8,
    fontWeight: "600"
},
accumulateBar: {
    flexDirection: "row",
    width: Dimensions.get('window').width*0.75,
    height: 30,
    marginHorizontal: 25,
    alignItems: "center"
},
accumulated: {
    flexDirection: "row",
    flex: 2,
    height: 17,
    alignItems: "center"
},
alreadyAccumulated: {
    width: '100%',
    height: 7,
    backgroundColor: '#114459',
    flexDirection: 'row',
    alignItems: 'center',
},
circleAccumulating: {
    width: '10%',
    height: 12,
    backgroundColor: '#114459',
    borderRadius: 6
},
noPtsYet: {
    flex: 1,
    height: 7,
    backgroundColor: '#EAC7A5',
    borderRadius: 3
},
remainingPts: {
    color: '#114459',
    fontSize: 13,
    fontWeight: "600",
    textAlign: "center"
},
accumulatingLogo:
{   position:'absolute',
    paddingTop: 25,
    paddingHorizontal: 15,
},
btnOptions: {
    marginTop: 10, flexDirection: "row",
    justifyContent: "space-between"
},
yrVoucher: {
    fontSize: 17,
     color: '#114459',
      fontWeight: "700",
      width:130
},


});

export default styles;
