import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  container: {
    alignItems:'center',
    marginTop:85,
     backgroundColor:'#114459E5',
     minHeight:600,
     maxHeight:841,
     minWidth:300,
     maxWidth:428,
     borderRadius:15,
     shadowColor:'grey'

  },
  createAccount: {
    fontSize: 20, color: '#BF946A',
    fontWeight: "700", marginTop: 19,
    width:100
  },

 
});

export default styles;
