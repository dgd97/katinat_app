import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import styles from "./styles";
import BtnComponent from "../../../components/BtnComponent";
import TxtInputComponent from "../../../components/TxtInputComponent";
export default function SignUp(props) {
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const [dob, setDob] = useState()
    const [gender, setGender] = useState()
    return (
        <View style={styles.container}>
            <BtnComponent size="btnBack" />
            <Text style={styles.createAccount}>Tạo tài khoản</Text>
            <TxtInputComponent label="firstName" value={firstName} onChangeText={setFirstName} />
            <TxtInputComponent label="lastName" value={lastName} onChangeText={setLastName} />
            <TxtInputComponent label="dob" value={dob} onChangeText={setDob} />
            <TxtInputComponent label="gender" value={gender} onChangeText={setGender} />
            <BtnComponent size="btnCreate" onPress={() => {
                props.navigation.navigate("HomeScr")
            }} />
        </View>
    )
}