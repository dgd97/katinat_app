import React, { useState } from "react";
import { TextInput, View, Text, Image, Dimensions } from "react-native";
import telephone from "../assets/icons/telephone.png"
import search from "../assets/icons/search.png"
//import { isValidEmail, isValidPhone } from "../../utils/Validations";

export default function TxtInputComponent(
    {
        err = 'black',
        disable = true,
        startIcon,
        row,
        isMultiline,
        label = '',
        secureText = false,
        value='',
        onChangeText,
        keyboardType
    }) {
    const getBorderColor = err === 'red' && 'red' || '#828282'
    const getBorder = () =>{
        
    }
    const getColor = () => {
        if (err === 'red') return 'red'
        if (label === 'email' || label === 'pwd') return '#828282'
        return '#333333'
    }
    const getHeight = () =>{
        if( label === 'firstName' || label === 'lastName'||
        label === 'dob' || label === 'gender') return 44
        if(label === 'phoneLI') return 48
        if(label === 'searchBar') return 40

    } 
      
    const getWidth = () => {
        if( label === 'phoneLI') return Dimensions.get('window').width*0.9
        if( label === 'firstName' || label === 'lastName'||
        label === 'dob' || label === 'gender') return Dimensions.get('window').width*0.8
        if(label === 'searchBar') return Dimensions.get("window").width * 0.59
    

        return 200
    }
    const getToptxt = row && 'top'
    const getBgColor = disable && 'white' || '#E0E0E0'
    const getPlaceHolder = () => {
        if (label === 'phoneLI') return 'Nhập số điện thoại'
        if (label === 'firstName') return 'Nhập tên của bạn *'
        if (label === 'lastName') return 'Nhập họ của bạn'
        if (label === 'dob') return 'Nhập ngày sinh của bạn'
        if (label === 'gender') return 'Giới tính'
        if (label === 'searchBar') return 'Tìm kiếm'

    }
    const getFontWeight = () =>{
        if( label === 'phoneLI' )
        return '400'
    }
    const getBdRadius = () => {
        if(label === 'phoneLI' ||
        label === 'searchBar') return 18
        if( label === 'firstName' || label === 'lastName'||
        label === 'dob' || label === 'gender') return 8
    }
    const getMarginTop = () => {
        if(label === 'phoneLI') return 30
        if(label === 'firstName') return 63
        if(label === 'lastName' || label ==='dob'||
        label ==='gender') return 33

    }
    const getMarginLeft = () =>{
        if(label === 'phoneLI') return 25
        if( label === 'firstName' || label === 'lastName'||
        label === 'dob' || label === 'gender') return 8
        if (label === 'searchBar') return 10

    }
    const getFontStyle = () =>{
        if(label === 'phoneLI') return "italic"

    }
    const getHeightTxt = () =>{
        if(label === 'phoneLI') return 16
    }
    const getPlaceHolderColor = () =>{
        if(label === 'firstName'||
        label ==='lastName'||
        label === 'dob'||
        label === 'gender') return '#5A5D5F'
        if(label === 'searchBar') return '#114459'
    }



    // State
    const [errorEmail, setErrorEmail] = useState('')
    const [errorPhone, setErrorPhone] = useState('')
    const getValid = () => {
        if (label === 'email' || label === 'emailCA')
            setErrorEmail(isValidEmail(value) == true ? '' : 'Email not in correct format')
        if (label === 'phone')
            setErrorPhone(isValidPhone(value) == true ? '' : 'Phone number is 10 digits')
    }
    return (
        

            <View style={{
                flexDirection: "row",
                alignItems: "center",
                width: getWidth(),
                borderRadius: getBdRadius(),
                height: getHeight(),
                backgroundColor: getBgColor,
                marginTop: getMarginTop()
            }}>
                {startIcon && <Image style={{height:22, width:13,
                marginLeft:13}}
                    source={telephone} />}
                    {label ==='searchBar' && <Image style={{
                marginLeft:13}}
                    source={search} />}
                <TextInput style={{
                    textAlignVertical: getToptxt,
                    marginLeft:getMarginLeft(),
                     fontStyle: getFontStyle(),
                    fontWeight:getFontWeight(),
                    
                }}
                    placeholder={getPlaceHolder()}
                    placeholderTextColor={getPlaceHolderColor()}
                    editable={disable}
                    value={value}
                    multiline={isMultiline}
                    numberOfLines={4}
                    secureTextEntry={secureText}
                    onChangeText={onChangeText}
                    onEndEditing ={ () => getValid(value)}
                    keyboardType={keyboardType}
                
                />
                {/* {endIcon === 'eye' && <Image style={[styles.endIconTxtInput, { marginLeft: getMagrinIcon }]} source={require('../../assets/images/eye.png')} /> ||
                    endIcon === 'lock' && <Image style={[styles.endIconTxtInput]} source={require('../../assets/images/lock.png')} />} */}
            </View>

        
    )
}