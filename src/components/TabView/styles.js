import { Dimensions, StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    banner: {
        width: Dimensions.get('window').width*0.9, height: 200,
        backgroundColor: 'white',
        borderRadius: 18,
        marginTop: 142,
        marginHorizontal:15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 0,
        
    },
    rank: {
        flexDirection: "row",
        marginTop: 70,
        paddingHorizontal: 10,
        elevation:13
    },
    txtRank: {
        color: 'black',
        fontSize: 8,
        fontWeight: "600"
    },
    accumulateBar: {
        flexDirection: "row",
        width: Dimensions.get('window').width*0.75,
        height: 30,
        marginHorizontal: 25,
        alignItems: "center"
    },
    accumulated: {
        flexDirection: "row",
        flex: 2,
        height: 17,
        alignItems: "center"
    },
    alreadyAccumulated: {
        width: '100%',
        height: 7,
        backgroundColor: '#114459',
        flexDirection: 'row',
        alignItems: 'center',
    },
    circleAccumulating: {
        width: '10%',
        height: 12,
        backgroundColor: '#114459',
        borderRadius: 6
    },
    noPtsYet: {
        flex: 1,
        height: 7,
        backgroundColor: '#EAC7A5',
        borderRadius: 3
    },
    remainingPts: {
        color: '#114459',
        fontSize: 13,
        fontWeight: "600",
        textAlign: "center"
    },
    accumulatingLogo:
    {   position:'absolute',
        paddingTop: 25,
        paddingHorizontal: 15,
    },
    btnOptions: {
        marginTop: 10, flexDirection: "row",
        justifyContent: "space-between"
    },
    yrVoucher: {
        fontSize: 17,
         color: '#114459',
          fontWeight: "700",
          width:150,
        paddingLeft:20
    },
});

export default styles;
