import React from "react";
import { View, Text, Image, Dimensions } from "react-native";
import TxtInputComponent from "../TxtInputComponent";
import AccumulatePtsLogo from "../../assets/images/AccumulatePtsLogo.png"
import BtnComponent from "../BtnComponent";
import styles from "./styles";
export default function Promotions() {
    return (
        <View style={{ paddingTop: 12, }}>
            <View style={styles.banner} >
                <View style={styles.rank}>
                    <Text style={styles.txtRank}>
                        BẠN MỚI
                    </Text>
                    <View style={{ flex: 1, alignItems: "flex-end" }}>
                        <Text style={styles.txtRank}>
                            ĐỒNG
                        </Text>
                    </View>

                </View>
                {/* AccumulateBar */}
                <View style={styles.accumulateBar}>
                    <View style={styles.accumulated}>

                        <View style={styles.alreadyAccumulated}>
                            <View style={{
                                flex: 1,
                            }}>
                                <View style={styles.circleAccumulating}>
                                </View>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <View style={styles.circleAccumulating}>
                                </View>
                            </View>
                        </View>

                    </View>
                    <View style={styles.noPtsYet}>
                    </View>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Text style={styles.remainingPts}>Còn 30 điểm nữa bạn sẽ thăng hạng Đồng.</Text>
                </View>
            </View>
            {/* QR LOGO */}
            <View style={styles.accumulatingLogo} >
                <Image style={{
                    width: Dimensions.get('window').width * 0.9,
                    height: 184,
                    borderRadius: 18,
                }} source={AccumulatePtsLogo} />
            </View>
            <View style={{ marginTop: 10, padding: 20 }}>
                <View style={{
                    flexDirection: "row", justifyContent: "space-between"
                }}>
                    <BtnComponent size="redeemOffer" />
                    <BtnComponent size="yrVoucher" />
                </View>
                <View style={styles.btnOptions}>
                    <BtnComponent size="beanHistory" />
                    <BtnComponent size="yrRights" />
                </View>
            </View>
            <View style={{
                flexDirection: "row", marginTop: 10, alignItems: "center"
            }}>
                <Text style={styles.yrVoucher}>Phiếu ưu đãi của bạn</Text>
            </View>
            <View style={{flexDirection:"row", paddingHorizontal:20,
        paddingTop:13, paddingBottom:8}}>
                <BtnComponent size="btnVoucherImg" />
                <BtnComponent size="btnVoucher" />
            </View>
            <View style={{flexDirection:"row", paddingHorizontal:20,
        paddingBottom:8}}>
                <BtnComponent size="btnVoucherImg" />
                <BtnComponent size="btnVoucher" />
            </View>
            <View style={{flexDirection:"row", paddingHorizontal:20,
        paddingBottom:8}}>
                <BtnComponent size="btnVoucherImg" />
                <BtnComponent size="btnVoucher" />
            </View>
            <View style={{flexDirection:"row", paddingHorizontal:20,
        paddingBottom:8}}>
                <BtnComponent size="btnVoucherImg" />
                <BtnComponent size="btnVoucher" />
            </View>

        </View>

    )
}
