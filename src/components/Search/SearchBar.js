import React from "react";
import { View, TextInput, Text, Image} from "react-native";
import styles from "./styles";
import search from "../../assets/icons/search.png"
import TxtInputComponent from "../TxtInputComponent";
export default function SearchBar ({onchangeText}) {
    return(
        <TxtInputComponent label='searchBar' 
        onchangeText={onchangeText}/>
    )
    
}