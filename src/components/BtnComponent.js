import { TouchableOpacity, Text, Image, Dimensions } from "react-native";
import React from "react";
import x from "../assets/icons/x.png"
import Back from "../assets/icons/back.png"
import Bell from "../assets/icons/bell.png"
import promos from "../assets/icons/promos.png"
import MilkCoffee from "../assets/images/Milk_Coffee.png"
import Circle from "../assets/images/Circle.png"
import gifts from "../assets/icons/gifts.png"
import vouchers from "../assets/icons/vouchers.png"
import history from "../assets/icons/history.png"
import shield from "../assets/icons/shield.png"
import Voucher from "../assets/images/Voucher.png"


const colorList = {
    txtColorDefault: '#3F3F3F',
    default: '#E0E0E0',
    noColor: null,
    white: 'white',
    blue: '#3D5AFE',
    primary: '#2962FF',
    primary1: '#0039CB',
    secondary: '#455A64',
    secondary1: '#1C313A',
    danger: '#D32F2F',
    danger1: '#9A0007',
    disableColor: '#9E9E9E',

}
export default function BtnComponent({
    size = '', disableShadow = false,
    disable, onPress,
}) {
    const getBgColor = () => {

        if (size === 'btnLogIn') return '#A8A8A8'
        if (size === 'btnX' || size === 'btnBack' ||
            size === 'btnResend' || size === 'btnAll' ||
            size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return null
        if (size === 'btnCreate') return "#5a5d5f9e"
        if (size === 'btnNoti' || size === 'btnPromos' ||
            size === 'Promotions' || size === 'Notifications' ||
            size === 'btnCoffee' ||
            size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights'||
            size === 'btnDeli' || size === 'btnVoucher' ||
            size === 'btnVoucherImg') return "white"
        if (size === 'btnPick' ||
            size === 'btnAdd') return '#114459'


        return colorList.default

    }
    const getTxtColor = () => {
        if (size === 'btnSignUp' || size === 'btnLogIn' ||
            size === 'btnCreate' || size === 'btnPick' ||
            size === 'btnAdd') return colorList.white
        if (size === 'btnResend' || size === 'btnAll') return "#BF946A"
        if (size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return '#114459'
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return "#323232"
        return colorList.txtColorDefault
    }
    const getBorder = () => {
    }
    const btnLabel = () => {
        if (size === 'btnLogIn') return 'Đăng nhập'
        if (size === 'btnResend') return ' Gửi lại '
        if (size === 'btnCreate') return 'Cập nhật'
        if (size === 'btnAll') return 'Tất cả'
        if (size === 'btnCoffee') return 'Cà phê sữa'
        if (size === 'btnPick') return 'Chọn'
        if (size === 'btnCircleCoffee') return 'Cà phê'
        if (size === 'btnCircleIceBlended') return 'Đá xay'
        if (size === 'btnCircleTea') return 'Trà'
        if (size === 'btnCircleMilkTea') return 'Trà sữa'
        if (size === 'btnCircleCake') return 'Bánh'
        if (size === 'btnAdd') return '+'
        if (size === 'redeemOffer') return 'Đổi ưu đãi'
        if (size === 'yrVoucher') return 'Voucher của bạn'
        if (size === 'beanHistory') return 'Lịch sử BEAN'
        if (size === 'yrRights') return 'Quyền lợi của bạn'
        if(size === 'btnDeli') return 'Giao đến'
        if(size === 'btnVoucher') return 'Khuyến mãi món mới'

    }


    const getWidth = () => {
        if (size === 'btnLogIn') return Dimensions.get('window').width*0.9
        if (size === 'btnX') return 25
        if (size === 'btnResend') return 45
        if (size === 'btnBack') return 12
        if (size === 'btnCreate') return Dimensions.get('window').width*0.8
        if (size === 'btnNoti' || size === 'btnPromos'
            || size === 'btnAdd' ||
            size === 'Promotions' ||
            size === 'Notifications') return 40
        if (size === 'btnAll') return 40
        if (size === 'btnCoffee') return 107
        if (size === 'btnPick') return 103
        if (size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return 71
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return Dimensions.get('window').width*0.4
        if(size === 'btnDeli') return 420
        if(size === 'btnVoucherImg') return Dimensions.get('window').width*0.3
        if(size === 'btnVoucher') return Dimensions.get('window').width*0.55
        


    }
    const getHeight = () => {
        if (size === 'btnLogIn') return 48
        if (size === 'btnX') return 25
        if (size === 'btnResend') return 23
        if (size === 'btnBack') return 8
        if (size === 'btnCreate') return 44
        if (size === 'btnNoti' || size === 'btnPromos'
            || size === 'btnAdd' ||
            size === 'Promotions' ||
            size === 'Notifications') return 40
        if (size === 'btnAll') return 20
        if (size === 'btnCoffee') return 178
        if (size === 'btnPick') return 30
        if (size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return 108
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return 67
        if(size === 'btnDeli') return 100
        if(size === 'btnVoucherImg' ||
        size === 'btnVoucher') return 107
        

    }
    const getWidthTxt = () => {
        if (size === 'btnLogIn') return 77
        if (size === 'btnAdd') return 14
        if (size === 'btnAll') return 40


    }
    const getHeightTxt = () => {
        if (size === 'btnAdd') return 36

    }
    const getMarginTxt = () => {
        if (size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return 8
    }


    const getBorderRadius = () => {
        if (size === 'btnLogIn' || size === 'btnPick') return 18
        if (size === 'btnCreate') return 8
        if (size === 'btnPromos' || size === 'btnNoti' ||
            size === 'Promotions' || size === 'Notifications') return 19
        if (size === 'btnCoffee') return 11
        if (size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return 11
        if (size === 'btnAdd') return 20
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return 15
    }
    const getFontSize = () => {
        if (size === 'btnResend' || size === 'btnPick' ||
        size === 'btnVoucher') return 16
        if (size === 'btnCreate' ||
            size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake' ||
            size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return 14
        if (size === 'btnAll') return 17
        if (size === 'btnAdd') return 30



    }
    const getFontWeight = () => {
        if (size === 'btnLogIn' || size === 'btnResend' ||
            size === 'btnPick' ||
            size === 'btnVoucher') return '400'
        if (size === 'btnCreate' ||
            size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return "500"
        if (size === 'btnAll') return "700"
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return "600"


    }
    const getMarginLeft = () => {
        if (size === 'btnCoffee') return 30
        if (size === 'btnPick') return 32
        if (size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake') return 30
        if (size === 'Promotions') return '47%'
        if(size === 'Notifications') return 10
        if(size === 'btnVoucher') return 1.5
    }
    const getMarginRight = () => {
    }
    const getMarginTop = () => {
        if (size === 'btnCreate') return 33
        if (size === 'btnLogIn') return 16
        if (size === 'btnPick') return 8
        if (size === 'btnAdd') return 10
    }
    const getPadding = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights'|| size ==='btnVoucher') return 10
    }
    const getPosition = () => {
        if (size === 'btnX' || size === 'btnBack' ||
            size === 'btnNoti' || size === "btnPromos"
            || size === "btnAll" ||
            size === 'btnDeli') return "absolute"
    }
    const getTop = () => {
        if (size === 'btnX') return 13
        if (size === 'btnBack') return 29
        if (size === 'btnNoti' ||
            size === 'btnPromos') return 9
        if (size === 'btnAll') return 270
    }
    const getBot = () => {
       if(size === 'btnDeli') return 0
    }
    const getRight = () => {
        if (size === 'btnX') return 33
        if (size === 'btnNoti') return 21
        if (size === 'btnPromos') return 77
        if (size === 'btnAll') return 20

    }
    const getLeft = () => {
        if (size === 'btnBack') return 34

    }
    const getFlexD = () => {
        if (size === 'btnCoffee' ||
            size === 'btnCircleCoffee' ||
            size === 'btnCircleIceBlended' ||
            size === 'btnCircleTea' ||
            size === 'btnCircleMilkTea' ||
            size === 'btnCircleCake' ||
            size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights' ||
            size === 'btnDeli' ||
            size === 'btnVoucher'
        ) return 'column'

        return 'row'

    }
    const getJustifyContent = () => {
        if (size === 'btnCoffee' || size === 'btnVoucher') return null
        return 'center'

    }
    const getAlignItems = () => {
        if(size === 'btnVoucher' ||
        size === 'btnLabel')  return null
        return 'center'
    }
    const getTextAlign = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights' ||
            size === 'btnVoucher') return null
        return 'center'
    }
    const getMarginTopTxt = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return 10

    }
    const getElevation = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights') return 3
        if(size === 'Promotions' || size === 'Notifications') return 0.75
    }
    const getShadowRadius = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights'||
            size ==='Promotions'||
            size === 'Notifications') return 16
    }
    const getShadowOpa = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights'||
            size === 'Promotions'||
            size === 'Notifications') return 0.99
    }
    const getShadowColor = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights'||
            size === 'Promotions' ||
            size === 'Notifications'||
            size === 'Notifications') return 'black'
    }
    const getShadowOffsetHeight = () => {
        if (size === 'redeemOffer' ||
            size === 'yrVoucher' ||
            size === 'beanHistory' ||
            size === 'yrRights'|| size === 'Promotions'||
            size === 'Notifications'||
            size === 'Notifications') return 12
    }
    const getFlex = () => {
        if (size === 'btnDeli'
           ) return 1
    }

    return (
        <TouchableOpacity
            style={{
                width: getWidth(), height: getHeight(),
                marginLeft: getMarginLeft(),
                marginTop: getMarginTop(),
                marginRight: getMarginRight(),
                padding: getPadding(),
                //FlexBox
                flex:getFlex(),
                position: getPosition(),
                top: getTop(),
                bottom: getBot(),
                right: getRight(),
                left: getLeft(),
                flexDirection: getFlexD(),
                alignItems: getAlignItems(),
                justifyContent: getJustifyContent(),

                borderColor: '#3D5AFE',
                backgroundColor: getBgColor(),
                borderWidth: getBorder(),
                borderRadius: getBorderRadius(),
                // Shadow
                shadowColor: getShadowColor(),
                shadowOffset: {
                    // width:10,
                    height: getShadowOffsetHeight(),
                },
                shadowOpacity: getShadowOpa(),
                shadowRadius: getShadowRadius(),

                elevation: getElevation(),


            }}
            disabled={disable}
            onPress={onPress}>
            {size === 'btnX' && <Image source={x} /> ||
                size === 'btnBack' && <Image source={Back} /> ||
                size === 'btnNoti' && <Image source={Bell} /> ||
                size === 'Notifications' && <Image source={Bell} /> ||

                size === 'btnPromos' && <Image source={promos} /> ||
                size === 'Promotions' && <Image source={promos} /> ||

                size === 'btnCoffee' && <Image source={MilkCoffee} /> ||
                size === 'btnCircleCoffee' && <Image source={Circle} /> ||
                size === 'btnCircleIceBlended' && <Image source={Circle} /> ||
                size === 'btnCircleTea' && <Image source={Circle} /> ||
                size === 'btnCircleMilkTea' && <Image source={Circle} /> ||
                size === 'btnCircleCake' && <Image source={Circle} /> ||
                size === 'redeemOffer' && <Image source={gifts} /> ||
                size === 'yrVoucher' && <Image source={vouchers} /> ||
                size === 'beanHistory' && <Image source={history} /> ||
                size === 'yrRights' && <Image source={shield} /> ||
                size === 'btnVoucherImg' && <Image source={Voucher}/>
            }

            {/* {startIcon && <Image source={require('../../assets/images/trolley.png')} /> ||
                size === 'btnBack' && <Image style={{ width: 25, height: 25 }} source={sourceIcon} />
                || size === 'btnEdit' && <Image style={{ width: 25, height: 25 }} source={sourceIcon} />
                || size === 'btnDel' && <Image style={{ width: 25, height: 25 }} source={sourceIcon} />} */}
            <Text style={{
                color: getTxtColor(),
                width: getWidthTxt(),
                height: getHeightTxt(),
                fontWeight: getFontWeight(),
                fontSize: getFontSize(),
                margin: getMarginTxt(),
                marginTop: getMarginTopTxt(),
                textAlign: getTextAlign(),
                
            }}>
                {btnLabel()}
            </Text>
        
            {size === 'btnVoucher' && <Text>Hết hạn: 30/01/2023</Text>}
            {/* {endIcon && <Image source={require('../../assets/images/trolley.png')} />} */}
        </TouchableOpacity>

    );
}




