import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LogIn from "../views/screens/LogIn/LogInScreen";
import SignUp from "../views/screens/SignUp/SignUpScreen";
import Home from "../views/screens/Home/HomeScreen";
export default function RootComponent (){
    const Stack = createNativeStackNavigator();

    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="LogIn" component={LogIn} />
                <Stack.Screen name="SignUp" component={SignUp} />
                <Stack.Screen name="HomeScr" component={Home} />
       

            </Stack.Navigator>
        </NavigationContainer>

    )
}